<?php

/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * @mainpage
 *
 * A responsive theme for the website @link http://pirckheimer-gymnasium.de
 * pirckheimer-gymnasium.de @endlink made with zen grids, based on drupal.
 *
 * @section colors Colors
 *
 * - orange: #d67c1c
 * - dark-blue: #485873
 * - light-blue: #78adb8
 * - green: #a2c037
 * - light-grey: #eee
 * - grey: #8a95a7
 * - dark-grey: #727b8a
 * - white: #fff
 *
 * - color-default: dark-grey
 * - color-1: orange
 * - color-2: dark-blue
 * - color-3: green
 * - color-4: light-blue
 *
 * @section fonts Fonts
 *
 * - font-normal: PGNSansRegular, Arial, Tahoma, Sans-serif
 * - font-bold: PGNSansBlack, Arial Black
 * - courier:  Courier New, DejaVu Sans Mono, monospace, sans-serif
 *
 * @section files Files
 *
 * @subsection settings Settings
 *
 * - @link template.php template.php @endlink
 * - @link theme-settings.php theme-settings.php @endlink
 *
 * @subsection templates Templates
 *
 * - @link templates/page.tpl.php page.tpl.php @endlink
 * - @link templates/node--pgn-group.tpl.php node--pgn-group.tpl.php @endlink
 * - @link templates/book-navigation.tpl.php book-navigation.tpl.php @endlink
 * - @link templates/panels-pane.tpl.php panels-pane.tpl.php @endlink
 */

/**
 * Implements hook_css_alter().
 */
function pgn_responsive_css_alter(&$css) {
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.dialog.css']);
  unset($css['misc/ui/jquery.ui.resizable.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css['modules/field/theme/field.css']);
  unset($css['modules/misc/ui/jquery.ui.button.css']);
  unset($css['modules/search/search.css']);
}


function pgn_responsive_process_page(&$variables) {
    global $user;
    if (!$user->uid) {
        drupal_add_js(drupal_get_path('theme', 'pgn_responsive') . '/js/remove_has_js.js');
    }
}
